package com.cookbook.app.dummy;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

//@Entity
@Getter
@Setter
@ToString
@Table(name = "tbl_Test"
//       , uniqueConstraints = @UniqueConstraint(
//        name = "id_unique",
//        columnNames = "id"
//)
)
//@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "test_id_sequence")
    @SequenceGenerator(name = "test_id_sequence", sequenceName = "test_id_sequence", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "ethnicity",columnDefinition = "character varying(30)")
    private String ethnicity;

    @Column(name = "sex",columnDefinition = "character varying(7)" , unique = true)
    private String sex;

//    public Test(Long id , Ethnicity ethnicity , Sex sex){
//        System.out.println("ALLsexxConstructor");
//        this.id = id;
//        this.ethnicity = ethnicity;
//        this.sex = sex;
//    }

//    public Test(){
//        System.out.println("NOARGsexxConstructor");
//    }

    public Test(String ethnicity , String sex){
        System.out.println("NORMALsexxConstructor");
        this.ethnicity = ethnicity;
        this.sex = sex;
    }
//    public Test(Ethnicity ethnicity , Sex sex){
//        System.out.println("NORMALsexxConstructor");
//        this.ethnicity = ethnicity;
//        this.sex = sex;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Test test = (Test) o;
        return id != null && Objects.equals(id, test.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
