package com.cookbook.app.dummy;

import com.cookbook.app.dummy.StudentRepository;
import com.cookbook.app.dummy.Student;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

//@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository){
        return args -> {
            Student shakthi = new Student(
                    "Shakthi",
                    "shakthi247365@gmail.com",
                    LocalDate.of(2001, Month.APRIL, 22)
                    /*21*/
            );
//            Student snegha = new Student(
//                    "Snegha",
//                    "snegha@gmail.com",
//                    LocalDate.of(1998, Month.DECEMBER, 30)
//                    /*23*/
//            );
            Student ram = new Student(
                    "Ram",
                    "ram@gmail.com",
                    LocalDate.of(1998, Month.DECEMBER, 30)
                    /*23*/
            );
            studentRepository.saveAll(List.of(shakthi , ram));
        };
    }
}
