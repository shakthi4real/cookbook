package com.cookbook.app.service.validator;

import java.util.regex.Pattern;

public interface Validator {

    static boolean isEmailValid(String email){
        //https://www.baeldung.com/java-email-validation-regex
        String emailRegex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})$";
        return Pattern.matches(emailRegex , email);
    }

    static boolean isFirstNameValid(String firstName){
        String firstNameRegex = "[A-Za-z]{1,15}";
        return Pattern.matches(firstNameRegex , firstName) && !firstName.contains(" ");
    }

    static boolean isMobileNumberValid(String mobileNumber) {
        String mobileNumberRegex = "[0-9]{10}";
        return Pattern.matches(mobileNumberRegex , mobileNumber);
    }
}
