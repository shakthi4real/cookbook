package com.cookbook.app.service.validator;

import com.cookbook.app.entity.Chef;

import java.util.ArrayList;
import java.util.function.BiFunction;

//Combinator pattern try
public interface ChefValidator extends BiFunction<Chef , ArrayList<String> , ArrayList<String>> {

    static ChefValidator isEmailValid() {
        return (chef , list) -> {
            if(!Validator.isEmailValid(chef.getEmail())){
                list.add("Email");
            }
            return list;
        };
    }

    static ChefValidator isFirstNameValid() {
        return (chef , list) -> {
            if(!Validator.isFirstNameValid(chef.getFirstName())){
                list.add("First name");
            }
            return list;
        };
    }

    static ChefValidator isMobileNumberValid() {
        return (chef , list) -> {
            if(!Validator.isMobileNumberValid(chef.getMobileNumber())){
                list.add("Mobile number");
            }
            return list;
        };
    }

    default ChefValidator and(ChefValidator other){
        return (chef, list) -> {
            ArrayList<String> finalList = this.apply(chef , new ArrayList<>());
            return finalList.isEmpty() ? other.apply(chef , finalList) : finalList;
        };
    }
}