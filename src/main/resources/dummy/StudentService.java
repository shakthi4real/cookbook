package dummy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

//    @Autowired
//    public StudentService(StudentRepository studentRepository) {
//        this.studentRepository = studentRepository;
//    }

    public List<Student> getStudents(){
        return studentRepository.findAll();
        //        return List.of(
//                new Student("Shakthi" ,
//                            "shakthi247365@gmail.com" ,
//                            LocalDate.of(2001 , Month.APRIL , 22) ,
//                            21),
//                new Student("Snegha" ,
//                            "snegha247365@gmail.com" ,
//                            LocalDate.of(2000 , Month.DECEMBER , 31) ,
//                            21)
//
//        );
    }

    public void addNewStudent(Student student) {
        studentRepository.save(student);
    }

    public void deleteStudent(Long id){
        Boolean idExists = studentRepository.existsById(id);
        if(!idExists){
            throw new IllegalStateException("Student with id " + id + " does not exist");
        }
//        studentRepository.deleteById(id);
    }

    public Student getStudentByEmailId(String emailId){
        Optional<Student> emailExists = studentRepository.findStudentByEmail(emailId);

        if(emailExists.isEmpty()){
            throw new IllegalStateException("Student with email " + emailId + " does not exist");
        }
        return studentRepository.findStudentByEmail(emailId).get();
    }

    public Student getStudentById(String id){
        Optional<Student> idExists = studentRepository.findStudentById(id);

        if(idExists.isEmpty()){
            throw new IllegalStateException("Student with id " + id + " does not exist");
        }
        return studentRepository.findStudentById(id).get();
    }
}