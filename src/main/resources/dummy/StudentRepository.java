package com.cookbook.app.dummy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("SELECT s FROM Student s WHERE s.emailId = ?1")
    Optional<Student> findStudentByEmail(String emailId);

    @Query("SELECT s FROM Student s WHERE s.id = ?1")
    Boolean studentExistsWithId(Long id);

    @Query("SELECT s FROM Student s WHERE s.emailId = ?1")
    Boolean studentExistsWithEmail(String emailId);

    @Query("SELECT s FROM Student s WHERE s.name = ?1")
    Boolean studentExistsWithName(String name);

//    @Query("SELECT s FROM Student s WHERE s.age = ?1")
//    Boolean studentExistsWithAge(Integer age);                              //challenging

    @Query("SELECT s FROM Student s WHERE s.id = 1000")
    Optional<Student> findStudentById(String id);

    List<Student> findAll();

    @Modifying
    @Transactional
    @Query(value = "update tbl_student set " +
            "name = :name," +
            "email_address =  :emailId," +
            "dob =  :dob," +
            "guardian_name = :guardianName," +
            "guardian_email = :guardianEmail," +
            "guardian_mobile = :guardianMobile" +
            "  where email_address = :queryEmailId" ,
            nativeQuery = true)
    void updateStudentByEmailId(@Param("queryEmailId") String queryEmailId ,
                                @Param("name") String name ,
                                @Param("emailId") String emailId ,
                                @Param("dob") LocalDate dob ,
                                @Param("guardianName") String guardianName ,
                                @Param("guardianEmail") String guardianEmail ,
                                @Param("guardianMobile") String guardianMobile);

}