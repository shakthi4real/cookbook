package com.cookbook.app.dummy;

import com.cookbook.app.dummy.Student;
import com.cookbook.app.dummy.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController//("students")
public class StudentController {
    @Autowired
    private StudentService studentService;

//    @Autowired
//    public StudentController(StudentService studentService) {
//        this.studentService = studentService;
//    }

    @GetMapping("/")
    public List<Student> getStudents(){
        return studentService.getStudents();
    }

    @PostMapping("add")
    public void registerNewStudent(@RequestBody Student student){
        studentService.addNewStudent(student);
    }

    //@PutMapping("")
    @DeleteMapping(path = "{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long studentId){
        studentService.deleteStudent(studentId);
    }
}