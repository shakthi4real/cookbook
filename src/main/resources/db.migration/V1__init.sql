CREATE SEQUENCE  IF NOT EXISTS chef_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE  IF NOT EXISTS cmmndsh_ordr_rvw_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE  IF NOT EXISTS common_dish_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE  IF NOT EXISTS customer_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE  IF NOT EXISTS order_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE  IF NOT EXISTS review_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE  IF NOT EXISTS special_dish_seq START WITH 1 INCREMENT BY 50;

CREATE TABLE chef (
  id BIGINT NOT NULL,
   firstname VARCHAR(25) NOT NULL,
   lastname VARCHAR(25),
   email VARCHAR(25) NOT NULL,
   dob date NOT NULL,
   mobile_number BIGINT NOT NULL,
   height SMALLINT,
   sex VARCHAR(6) NOT NULL,
   ethnicity VARCHAR(25) NOT NULL,
   description VARCHAR(255),
   address VARCHAR(255) NOT NULL,
   CONSTRAINT pk_chef PRIMARY KEY (id)
);

CREATE TABLE cmmndsh_ordr_rvw (
  id BIGINT NOT NULL,
   common_dish_id BIGINT NOT NULL,
   order_id BIGINT NOT NULL,
   review_id BIGINT NOT NULL,
   CONSTRAINT pk_cmmndsh_ordr_rvw PRIMARY KEY (id)
);

CREATE TABLE common_dish (
  id BIGINT NOT NULL,
   name VARCHAR(50) NOT NULL,
   description VARCHAR(255),
   CONSTRAINT pk_common_dish PRIMARY KEY (id)
);

CREATE TABLE customer (
  id BIGINT NOT NULL,
   firstname VARCHAR(25) NOT NULL,
   lastname VARCHAR(25),
   email VARCHAR(25) NOT NULL,
   dob date NOT NULL,
   mobile_number BIGINT NOT NULL,
   sex VARCHAR(6),
   ethnicity VARCHAR(25),
   kitchen_details VARCHAR(255),
   expectations_from_chef VARCHAR(255),
   address VARCHAR(255) NOT NULL,
   CONSTRAINT pk_customer PRIMARY KEY (id)
);

CREATE TABLE order_tbl (
  id BIGINT NOT NULL,
   time_of_order TIMESTAMP WITHOUT TIME ZONE NOT NULL,
   order_status VARCHAR(25) NOT NULL,
   description VARCHAR(255),
   customer_id BIGINT NOT NULL,
   chef_id BIGINT NOT NULL,
   CONSTRAINT pk_order_tbl PRIMARY KEY (id)
);

CREATE TABLE review (
  id BIGINT NOT NULL,
   star VARCHAR(1) NOT NULL,
   description VARCHAR(255),
   CONSTRAINT pk_review PRIMARY KEY (id)
);

CREATE TABLE special_dish (
  id BIGINT NOT NULL,
   name VARCHAR(25) NOT NULL,
   chef_id BIGINT NOT NULL,
   description VARCHAR(255),
   CONSTRAINT pk_special_dish PRIMARY KEY (id)
);

ALTER TABLE chef ADD CONSTRAINT uc_chef_email UNIQUE (email);

ALTER TABLE cmmndsh_ordr_rvw ADD CONSTRAINT uc_cmmndsh_ordr_rvw_review UNIQUE (review_id);

ALTER TABLE common_dish ADD CONSTRAINT uc_common_dish_name UNIQUE (name);

ALTER TABLE customer ADD CONSTRAINT uc_customer_email UNIQUE (email);

ALTER TABLE special_dish ADD CONSTRAINT uc_special_dish_chef UNIQUE (chef_id);

ALTER TABLE special_dish ADD CONSTRAINT uc_special_dish_name UNIQUE (name);

ALTER TABLE cmmndsh_ordr_rvw ADD CONSTRAINT FK_CMMNDSH_ORDR_RVW_ON_COMMON_DISH FOREIGN KEY (common_dish_id) REFERENCES common_dish (id);

ALTER TABLE cmmndsh_ordr_rvw ADD CONSTRAINT FK_CMMNDSH_ORDR_RVW_ON_ORDER FOREIGN KEY (order_id) REFERENCES order_tbl (id);

ALTER TABLE cmmndsh_ordr_rvw ADD CONSTRAINT FK_CMMNDSH_ORDR_RVW_ON_REVIEW FOREIGN KEY (review_id) REFERENCES review (id);

ALTER TABLE order_tbl ADD CONSTRAINT FK_ORDER_TBL_ON_CHEF FOREIGN KEY (chef_id) REFERENCES chef (id);

ALTER TABLE order_tbl ADD CONSTRAINT FK_ORDER_TBL_ON_CUSTOMER FOREIGN KEY (customer_id) REFERENCES customer (id);

ALTER TABLE special_dish ADD CONSTRAINT FK_SPECIAL_DISH_ON_CHEF FOREIGN KEY (chef_id) REFERENCES chef (id);