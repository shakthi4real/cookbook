package com.cookbook.app.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "special_dish"
        ,uniqueConstraints = {
        @UniqueConstraint(
                name = "uc_special_dish_chef",
                columnNames = "chef_id"
        ),@UniqueConstraint(
                name = "uc_special_dish_name",
                columnNames = "name"
)
//uc name not mapped
})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpecialDish {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "special_dish_seq")
    @SequenceGenerator(name = "special_dish_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, columnDefinition = "character varying(25)")
    private String name;

    @OneToOne(cascade = CascadeType.ALL, optional = false, orphanRemoval = true)
    @JoinColumn(name = "chef_id", nullable = false, unique = true , foreignKey = @ForeignKey(name="FK_SPECIAL_DISH_ON_CHEF"))
    private Chef chef;

    @Column(name = "description" , columnDefinition = "character varying(255)")
    private String description;

    public Chef getChef() {
        return chef;
    }

    public void setChef(Chef chef) {
        this.chef = chef;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}