package com.cookbook.app.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "cmmndsh_ordr_rvw"
        ,uniqueConstraints = @UniqueConstraint(
                name = "uc_cmmndsh_ordr_rvw_review",
                columnNames = "review_id"
)
)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CmmnDshOrdrRvw {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cmmndsh_ordr_rvw_seq")
    @SequenceGenerator(name = "cmmndsh_ordr_rvw_seq", sequenceName = "cmmndsh_ordr_rvw_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "common_dish_id", nullable = false , foreignKey = @ForeignKey(name="FK_CMMNDSH_ORDR_RVW_ON_COMMON_DISH"))
    private CommonDish commonDish;

    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "order_id", nullable = false , foreignKey = @ForeignKey(name="FK_CMMNDSH_ORDR_RVW_ON_ORDER"))
    private Order order;

    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "review_id", nullable = false, unique = true , foreignKey = @ForeignKey(name="FK_CMMNDSH_ORDR_RVW_ON_REVIEW"))
    private Review review;

    public void setCommonDish(CommonDish commonDish) {
        this.commonDish = commonDish;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public Order getOrder() {
        return order;
    }

    public CommonDish getCommonDish() {
        return commonDish;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}