package com.cookbook.app.entity;

import com.cookbook.app.enumConstant.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "order_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq")
    @SequenceGenerator(name = "order_seq", sequenceName = "order_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "time_of_order", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private Timestamp timeOfOrder;

    @Column(name = "order_status" , columnDefinition = "character varying(25)" , nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Column(name = "description" , columnDefinition = "character varying(255)")
    private String description;

    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "customer_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "FK_ORDER_TBL_ON_CUSTOMER"))
    private Customer customer;

    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "chef_id", nullable = false, updatable = false, foreignKey = @ForeignKey(name = "FK_ORDER_TBL_ON_CHEF"))
    private Chef chef;
}