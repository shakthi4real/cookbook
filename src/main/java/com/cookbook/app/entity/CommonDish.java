package com.cookbook.app.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "common_dish"
        ,uniqueConstraints = @UniqueConstraint(
                name = "uc_common_dish_name",
                columnNames = "name"
))
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CommonDish {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "common_dish_seq")
    @SequenceGenerator(name = "common_dish_seq", sequenceName = "common_dish_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, columnDefinition = "character varying(50)")
    private String name;

    @Column(name = "description" , columnDefinition = "character varying(255)")
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}