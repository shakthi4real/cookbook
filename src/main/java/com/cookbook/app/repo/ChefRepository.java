package com.cookbook.app.repo;

import com.cookbook.app.entity.Chef;
import com.cookbook.app.enumConstant.Ethnicity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface ChefRepository extends JpaRepository<Chef, Long> {
    Optional<Chef> findByEmail(String email);

    Optional<Chef> findByMobileNumber(String mobileNumber);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM chef WHERE email= ?1",
            nativeQuery = true)
    void deleteByEmail(String email);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM chef WHERE mobile_number= ?1",
            nativeQuery = true)
    void deleteByMobileNumber(String mobileNumber);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET email = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateEmailByEmail(String email , String updatedEmail);

//    @Modifying
//    @Transactional
//    @Query(value = "UPDATE chef SET description = :description WHERE email= :email",
//            nativeQuery = true)
//    void updateDescriptionByEmail(@Param("email")String email , @Param("description")String description);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET mobile_number = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateNumberByEmail(String email , String updatedNumber);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET address = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateChefAddress(String email , String address);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET description = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateChefDescription(String email , String description);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET ethnicity = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateChefEthnicity(String email , String ethnicity);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET height = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateChefHeight(String email , String height);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET firstname = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateChefFirstName(String email , String firstname);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET lastname = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateChefLastName(String email , String lastname);

    @Modifying
    @Transactional
    @Query(value = "UPDATE chef SET sex = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateChefSex(String email , String sex);

    //try
    List<Chef> findByEmailAndMobileNumber(String email , String mobileNumber);
    List<Chef> findByEmailAndMobileNumberAndFirstName(String email , String mobileNumber , String firstName);
}