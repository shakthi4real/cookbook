package com.cookbook.app.repo;

import com.cookbook.app.entity.Order;
import com.cookbook.app.enumConstant.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByCustomerId(Long customerId);
    List<Order> findByChefId(Long chefId);
    List<Order> findByChefIdAndCustomerId(Long chefId , Long customerId);
    List<Order> findByOrderStatus(String status);
    List<Order> findByChefIdAndOrderStatus(Long chefId , String status);
    List<Order> findByCustomerIdAndOrderStatus(Long customerId , String status);
    List<Order> findByChefIdAndCustomerIdAndOrderStatus(Long chefId , Long customerId , String status);
    List<Order> findByTimeOfOrderBetween(Timestamp from , Timestamp to);
    List<Order> findByChefIdAndTimeOfOrderBetween(Long chefId , Timestamp from , Timestamp to);
    List<Order> findByCustomerIdAndTimeOfOrderBetween(Long customerId , Timestamp from , Timestamp to);
    List<Order> findByChefIdAndCustomerIdAndTimeOfOrderBetween(Long chefId , Long customerId , Timestamp from , Timestamp to);

    @Modifying
    @Transactional
    @Query(value = "UPDATE order_tbl SET description = ?2 WHERE id= ?1",
            nativeQuery = true)
    void updateOrderDescription(Long id , String updatedDescription);

    @Modifying
    @Transactional
    @Query(value = "UPDATE order_tbl SET order_status = ?2 WHERE id= ?1",
            nativeQuery = true)
    void updateOrderOrderStatus(Long id , String updatedOrderStatus);
}