package com.cookbook.app.repo;

import com.cookbook.app.entity.SpecialDish;
import com.cookbook.app.entity.SpecialDish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;

public interface SpecialDishRepository extends JpaRepository<SpecialDish, Long> {
    Optional<SpecialDish> findByName(String name);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM special_dish WHERE name= ?1",
            nativeQuery = true)
    void deleteByName(String name);

    @Modifying
    @Transactional
    @Query(value = "UPDATE special_dish SET name = ?2 WHERE name= ?1",
            nativeQuery = true)
    void updateNameByName(String name , String updatedName);

    @Modifying
    @Transactional
    @Query(value = "UPDATE special_dish SET description = ?2 WHERE name= ?1",
            nativeQuery = true)
    void updateDescriptionByName(String name , String updatedDescription);


    @Modifying
    @Transactional
    @Query(value = "UPDATE special_dish SET chef_id = ?2 WHERE name= ?1",
            nativeQuery = true)
    void updateChefIdByName(String name , String chefId);
}