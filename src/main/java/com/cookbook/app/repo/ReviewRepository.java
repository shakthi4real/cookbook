package com.cookbook.app.repo;

import com.cookbook.app.entity.Review;
import com.cookbook.app.entity.SpecialDish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;

public interface ReviewRepository extends JpaRepository<Review, Long> {
    
//    Optional<Review> findbyId(Long id);

    @Modifying
    @Transactional
    @Query(value = "UPDATE review SET description = ?2 WHERE id= ?1",
            nativeQuery = true)
    void updateDescriptionById(Long id , String updatedDescription);

    @Modifying
    @Transactional
    @Query(value = "UPDATE review SET star = ?2 WHERE id= ?1",
            nativeQuery = true)
    void updateStarById(Long id , String star);
}