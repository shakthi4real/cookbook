package com.cookbook.app.repo;

import com.cookbook.app.entity.Customer;
import com.cookbook.app.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByEmail(String email);

    Optional<Customer> findByMobileNumber(String mobileNumber);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM customer WHERE email= ?1",
            nativeQuery = true)
    void deleteByEmail(String email);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM customer WHERE mobile_number= ?1",
            nativeQuery = true)
    void deleteByMobileNumber(String mobileNumber);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET email = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateEmailByEmail(String email , String updatedEmail);
    
    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET mobile_number = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateNumberByEmail(String email , String updatedNumber);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET address = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateCustomerAddress(String email , String address);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET kitchen_details = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateCustomerKitchenDetails(String email , String kitchen_details);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET ethnicity = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateCustomerEthnicity(String email , String ethnicity);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET height = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateCustomerHeight(String email , String height);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET firstname = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateCustomerFirstName(String email , String firstname);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET lastname = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateCustomerLastName(String email , String lastname);

    @Modifying
    @Transactional
    @Query(value = "UPDATE customer SET sex = ?2 WHERE email= ?1",
            nativeQuery = true)
    void updateCustomerSex(String email , String sex);
}