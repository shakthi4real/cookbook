package com.cookbook.app.repo;

import com.cookbook.app.entity.CommonDish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.Optional;

public interface CommonDishRepository extends JpaRepository<CommonDish, Long> {
    Optional<CommonDish> findByName(String name);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM common_dish WHERE name= ?1",
            nativeQuery = true)
    void deleteByName(String name);

    @Modifying
    @Transactional
    @Query(value = "UPDATE common_dish SET name = ?2 WHERE name= ?1",
            nativeQuery = true)
    void updateNameByName(String name , String updatedName);

    @Modifying
    @Transactional
    @Query(value = "UPDATE common_dish SET description = ?2 WHERE name= ?1",
            nativeQuery = true)
    void updateDescriptionByName(String name , String updatedDescription);
}