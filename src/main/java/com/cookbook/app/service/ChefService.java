package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Chef;
import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;

import java.util.List;

public interface ChefService {
    List<Chef> getAllChefs();
    Chef getChefByEmail(String email) throws ItemNotFoundException;
    Chef getChefByMobileNumber(String mobileNumber) throws ItemNotFoundException;
    void saveChef(Chef chef) throws DuplicateDataException;
    void updateChefEmail(String email,String updatedEmail) throws DuplicateDataException;
    void updateChefMobileNumber(String email , String updatedNumber) throws DuplicateDataException;
    void updateChefAddress(String email , String updatedAddress);
    void updateChefDescription(String email , String updatedDescription);
    void updateChefEthnicity(String email , Ethnicity updatedEthnicity);
    void updateChefHeight(String email , String updatedHeight);
    void updateChefFirstName(String email , String updatedFirstname);
    void updateChefLastName(String email , String updatedLastname);
    void updateChefSex(String email , Sex updatedSex);
    void deleteByEmail(String email);
    void deleteByMobileNumber(String number);
}
