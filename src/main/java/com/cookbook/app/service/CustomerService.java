package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Customer;
import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers();
    Customer getCustomerByEmail(String email) throws ItemNotFoundException;
    Customer getCustomerByMobileNumber(String mobileNumber) throws ItemNotFoundException;
    void saveCustomer(Customer customer) throws DuplicateDataException;
    void updateCustomerEmail(String email,String updatedEmail) throws DuplicateDataException;
    void updateCustomerMobileNumber(String email , String updatedNumber) throws DuplicateDataException;
    void updateCustomerAddress(String email , String updatedAddress);
    void updateCustomerKitchenDetails(String email , String updatedKitchenDetails);
    void updateCustomerEthnicity(String email , Ethnicity updatedEthnicity);
    void updateCustomerHeight(String email , String updatedHeight);
    void updateCustomerFirstName(String email , String updatedFirstname);
    void updateCustomerLastName(String email , String updatedLastname);
    void updateCustomerSex(String email , Sex updatedSex);
    void deleteByEmail(String email);
    void deleteByMobileNumber(String number);
}
