package com.cookbook.app.service;

import com.cookbook.app.entity.Order;
import com.cookbook.app.enumConstant.OrderStatus;

import java.sql.Timestamp;
import java.util.List;

public interface OrderService {
    List<Order> getAllOrders();
    List<Order> getOrderByCustomerId(Long customerId);
    List<Order> getOrderByChefId(Long chefId);
    List<Order> getOrderByChefIdAndCustomerId(Long chefId , Long customerId);
    List<Order> getOrderByOrderStatus(OrderStatus orderStatus);
    List<Order> getOrderByChefIdAndStatus(Long chefId , OrderStatus orderStatus);
    List<Order> getOrderByCustomerIdAndStatus(Long customerId , OrderStatus orderStatus);
    List<Order> getOrderByChefIdCustomerIdAndStatus(Long chefId , Long customerId , OrderStatus orderStatus);
    List<Order> getOrderBetween(Timestamp from , Timestamp to);
    List<Order> getOrderByChefIdBetween(Long chefId , Timestamp from , Timestamp to);
    List<Order> getOrderByCustomerIdBetween(Long customerId , Timestamp from , Timestamp to);
    List<Order> getOrderByChefIdAndCustomerIdBetween(Long chefId , Long customerId , Timestamp from , Timestamp to);
    void updateOrderDescription(Long id , String updatedDescription);
    void updateOrderOrderStatus(Long id , String updatedOrderStatus);
}
