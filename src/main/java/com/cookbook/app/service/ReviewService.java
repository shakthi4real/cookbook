package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Review;
import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;
import com.cookbook.app.enumConstant.Star;

import java.util.List;

public interface ReviewService {
    List<Review> getAllReviews();
    Review getReviewById(Long id) throws ItemNotFoundException;
    void saveReview(Review review);
    void updateReviewDescription(Long id , String updatedDescription);
    void updateReviewStar(Long id , Star star);
    void deleteById(Long id);
}
