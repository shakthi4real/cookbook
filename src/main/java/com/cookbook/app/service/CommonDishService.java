package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.CommonDish;

import java.util.List;

public interface CommonDishService {
    List<CommonDish> getAllCommonDishes();
    CommonDish getCommonDishByName(String name) throws ItemNotFoundException;
    void saveCommonDish(CommonDish commonDish) throws DuplicateDataException;
    void updateCommonDishName(String name , String updatedName) throws DuplicateDataException;
    void updateCommonDishDescription(String name , String updatedDescription);
    void deleteByName(String name);
}
