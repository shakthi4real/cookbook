package com.cookbook.app.service;

import com.cookbook.app.entity.CmmnDshOrdrRvw;
import com.cookbook.app.repo.CmmnDshOrdrRvwRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CmmnDshOrdrRvwServiceImpl implements CmmnDshOrdrRvwService {

    @Autowired
    private CmmnDshOrdrRvwRepository cmmnDshOrdrRvwRepository;

    @Override
    public List<CmmnDshOrdrRvw> getAllCmmnDshOrdrRvws() {
        return cmmnDshOrdrRvwRepository.findAll();
    }

    @Override
    public List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByReviewId(Long reviewId) {
        return cmmnDshOrdrRvwRepository.findByReviewId(reviewId);
    }

    @Override
    public List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByOrderId(Long orderId) {
        return cmmnDshOrdrRvwRepository.findByOrderId(orderId);
    }

    @Override
    public List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByOrderIdAndCommonDishId(Long orderId, Long commonDishId) {
        return cmmnDshOrdrRvwRepository.findByOrderIdAndCommonDishId(orderId , commonDishId);
    }
}
