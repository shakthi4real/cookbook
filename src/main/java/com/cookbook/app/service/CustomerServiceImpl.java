package com.cookbook.app.service;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Customer;
import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;
import com.cookbook.app.repo.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getCustomerByEmail(String email) throws ItemNotFoundException {
        return customerRepository.findByEmail(email)
                .orElseThrow(() -> new ItemNotFoundException("Customer with email : " + email + " is not present"));
    }

    @Override
    public Customer getCustomerByMobileNumber(String mobileNumber) throws ItemNotFoundException {
        return customerRepository.findByMobileNumber(mobileNumber)
                .orElseThrow(() -> new ItemNotFoundException("Customer with mobile number : " + mobileNumber + " is not present"));
    }

    @Override
    public void saveCustomer(Customer customer) throws DuplicateDataException {
        try{customerRepository.save(customer);}
        catch (Exception exception){
            StringBuffer message = new  StringBuffer("Customer with");
            customerRepository.findByEmail(customer.getEmail())
                    .ifPresent(customer1 ->  message.append(" email:" + customer1.getEmail()));

            customerRepository.findByMobileNumber(customer.getMobileNumber())
                    .ifPresent(customer2 -> {
                        if(!message.toString().equals("Customer with")) message.append(" and");
                        message.append(" mobile Number:" + customer2.getMobileNumber());
                    });
            message.append(" already exists");
            throw new DuplicateDataException(message.toString());
        };
    }

    @Override
    public void updateCustomerEmail(String email, String updatedEmail) throws DuplicateDataException {
        try{customerRepository.updateEmailByEmail(email , updatedEmail);}
        catch (Exception exception){
            throw new DuplicateDataException("Email : " + updatedEmail + " is taken by some other Customer. Try giving another email");
        }
    }

    @Override
    public void updateCustomerMobileNumber(String email, String updatedNumber) throws DuplicateDataException {
        try{customerRepository.updateNumberByEmail(email , updatedNumber);}
        catch (Exception exception){
            throw new DuplicateDataException("Mobile number : " + updatedNumber + " is taken by some other Customer. Try giving another number");
        }
    }

    @Override
    public void updateCustomerAddress(String email, String updatedAddress) {
        customerRepository.updateCustomerAddress(email , updatedAddress);
    }

    @Override
    public void updateCustomerKitchenDetails(String email, String updatedKitchenDetails) {
        customerRepository.updateCustomerKitchenDetails(email , updatedKitchenDetails);
    }

    @Override
    public void updateCustomerEthnicity(String email, Ethnicity updatedEthnicity) {
        customerRepository.updateCustomerEthnicity(email , updatedEthnicity.toString());
    }

    @Override
    public void updateCustomerHeight(String email, String updatedHeight) {
        customerRepository.updateCustomerHeight(email , updatedHeight);
    }

    @Override
    public void updateCustomerFirstName(String email, String updatedFirstname) {
        customerRepository.updateCustomerFirstName(email , updatedFirstname);
    }

    @Override
    public void updateCustomerLastName(String email, String updatedLastname) {
        customerRepository.updateCustomerLastName(email , updatedLastname);
    }

    @Override
    public void updateCustomerSex(String email, Sex updatedSex) {
        customerRepository.updateCustomerSex(email , updatedSex.toString());
    }

    @Override
    public void deleteByEmail(String email) {
        customerRepository.deleteByEmail(email);
    }

    @Override
    public void deleteByMobileNumber(String number) {
        customerRepository.deleteByMobileNumber(number);
    }
}
