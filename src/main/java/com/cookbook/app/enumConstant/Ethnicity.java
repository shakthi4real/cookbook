package com.cookbook.app.enumConstant;

public enum Ethnicity {
    ASIAN("Asian"),
    AFRICAN("African"),
    JAPANESE("Japanese"),
    CHINESE("Chinese"),
    EUROPEAN("European");

    private final String value;

    Ethnicity(String value) {
        this.value = value;
    }

    public String get(){
        return value;
    }
}
