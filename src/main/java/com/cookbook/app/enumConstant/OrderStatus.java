package com.cookbook.app.enumConstant;

public enum OrderStatus {
    BOOKED("Booked"),
    ONGOING("Ongoing"),
    COMPLETED("Completed"),
    CANCELLED("Cancelled");

    private final String value;

    OrderStatus(String value) {
        this.value = value;
    }

    public String get(){
        return value;
    }
}
