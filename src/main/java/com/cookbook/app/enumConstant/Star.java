package com.cookbook.app.enumConstant;

public enum Star {
    ONE("1") ,
    TWO("2") ,
    THREE("3") ,
    FOUR("4") ,
    FIVE("5");

    private final String value;

    Star(String value) {
        this.value = value;
    }

    public String get(){
        return value;
    }
}
