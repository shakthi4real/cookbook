package com.cookbook.app.enumConstant;

public enum ChefValidationStatus {
    SUCCESS,
    EMAIL_NOT_GIVEN,
    LAST_NAME_NOT_GIVEN,
    DOB_NOT_GIVEN,
    MOBILE_NUMBER_NOT_GIVEN,
    SEX_NOT_GIVEN,
    ETHNICITY_NOT_GIVEN,
    ADDRESS_NOT_GIVEN,
}
