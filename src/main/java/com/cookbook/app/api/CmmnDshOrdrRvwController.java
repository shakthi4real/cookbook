package com.cookbook.app.api;

import com.cookbook.app.entity.CmmnDshOrdrRvw;
import com.cookbook.app.service.CmmnDshOrdrRvwService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("cmmnDshOrdrRvw")
public class CmmnDshOrdrRvwController {

    @Autowired
    private CmmnDshOrdrRvwService cmmnDshOrdrRvwService;

    @GetMapping
    public List<CmmnDshOrdrRvw> getAllCmmnDshOrdrRvws() {
        return cmmnDshOrdrRvwService.getAllCmmnDshOrdrRvws();
    }

    @GetMapping("/reviewId/{reviewId}")
    public List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByReviewId(@PathVariable("reviewId") Long reviewId) {
        return cmmnDshOrdrRvwService.getCmmnDshOrdrRvwByReviewId(reviewId);
    }

    @GetMapping("/orderId/{orderId}")
    public List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByOrderId(@PathVariable("orderId") Long orderId) {
        return cmmnDshOrdrRvwService.getCmmnDshOrdrRvwByOrderId(orderId);
    }

    @GetMapping("/orderIdAndCommonDishId")
    public List<CmmnDshOrdrRvw> getCmmnDshOrdrRvwByOrderIdAndCommonDishId(@PathParam(value = "orderId") Long orderId, @PathParam(value = "commonDishId") Long commonDishId) {
        return cmmnDshOrdrRvwService.getCmmnDshOrdrRvwByOrderIdAndCommonDishId(orderId , commonDishId);
    }
}