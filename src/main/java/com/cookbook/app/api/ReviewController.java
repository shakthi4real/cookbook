package com.cookbook.app.api;

import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Review;
import com.cookbook.app.enumConstant.Star;
import com.cookbook.app.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("review")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @GetMapping
    public List<Review> getAllReviews() {
        return reviewService.getAllReviews();
    }

    @GetMapping("/id/{id}")
    public Review getReviewById(@PathVariable("id") Long id) throws ItemNotFoundException {
        return reviewService.getReviewById(id);
    }

    @PostMapping
    public void saveReview(@RequestBody Review review) {
        reviewService.saveReview(review);
    }

    @PutMapping("/description")
    public void updateReviewDescription(@RequestParam(value = "id") Long id, @RequestParam(value = "updatedDescription") String updatedDescription) {
        reviewService.updateReviewDescription(id , updatedDescription);
    }

    @PutMapping("/star")
    public void updateReviewStar(@RequestParam(value = "id") Long id, @RequestParam(value = "star") Star star) {
        reviewService.updateReviewStar(id , star);
    }

    @DeleteMapping
    public void deleteById(Long id) {
        reviewService.deleteById(id);
    }
}
