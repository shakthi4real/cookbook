package com.cookbook.app.api;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Customer;
import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;
import com.cookbook.app.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @GetMapping("/email/{email}")
    public Customer getCustomerByEmail(@PathVariable("email") String email) throws ItemNotFoundException {
        return customerService.getCustomerByEmail(email);
    }

    @GetMapping("/mobileNumber/{mobileNumber}")
    public Customer getCustomerByMobileNumber(@PathVariable("mobileNumber") String mobileNumber) throws ItemNotFoundException {
        return customerService.getCustomerByMobileNumber(mobileNumber);
    }

    @PostMapping
    public void saveCustomer(@RequestBody Customer customer) throws DuplicateDataException {
        customerService.saveCustomer(customer);
    }

    @PutMapping("/email")
    public void updateCustomerEmail(@RequestParam(value = "email") String email, @RequestParam(value = "updatedEmail") String updatedEmail) throws DuplicateDataException {
        customerService.updateCustomerEmail(email,updatedEmail);
    }

    @PutMapping("/mobileNumber")
    public void updateCustomerMobileNumber(@RequestParam(value = "email") String email, @RequestParam(value = "updatedNumber") String updatedNumber) throws DuplicateDataException {
        customerService.updateCustomerMobileNumber(email , updatedNumber);
    }

    @PutMapping("/address")
    public void updateCustomerAddress(@RequestParam(value = "email") String email, @RequestParam(value = "updatedAddress") String updatedAddress) {
        customerService.updateCustomerAddress(email , updatedAddress);
    }

    @PutMapping("/kitchenDetails")
    public void updateCustomerKitchenDetails(@RequestParam(value = "email") String email, @RequestParam(value = "updatedKitchenDetails") String updatedKitchenDetails) {
        customerService.updateCustomerKitchenDetails(email , updatedKitchenDetails);
    }

    @PutMapping("/ethnicity")
    public void updateCustomerEthnicity(@RequestParam(value = "email") String email, @RequestParam(value = "updatedEthnicity") Ethnicity updatedEthnicity) {
        customerService.updateCustomerEthnicity(email, updatedEthnicity);
    }

    @PutMapping("/height")
    public void updateCustomerHeight(@RequestParam(value = "email") String email, @RequestParam(value = "updatedHeight") String updatedHeight) {
        customerService.updateCustomerHeight(email , updatedHeight);
    }

    @PutMapping("/firstname")
    public void updateCustomerFirstName(@RequestParam(value = "email") String email, @RequestParam(value = "updatedFirstname") String updatedFirstname) {
        customerService.updateCustomerFirstName(email, updatedFirstname);
    }

    @PutMapping("/lastname")
    public void updateCustomerLastName(@RequestParam(value = "email") String email, @RequestParam(value = "updatedLastname") String updatedLastname) {
        customerService.updateCustomerLastName(email, updatedLastname);
    }

    @PutMapping("/sex")
    public void updateCustomerSex(@RequestParam(value = "email") String email, @RequestParam(value = "updatedSex") Sex updatedSex) {
        customerService.updateCustomerSex(email, updatedSex);
    }

    @DeleteMapping("/email")
    public void deleteByEmail(@RequestParam(value = "email") String email) {
        customerService.deleteByEmail(email);
    }

    @DeleteMapping("/mobileNumber")
    public void deleteByMobileNumber(@RequestParam(value = "mobileNumber") String number) {
        customerService.deleteByMobileNumber(number);
    }
}