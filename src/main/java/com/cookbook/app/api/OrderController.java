package com.cookbook.app.api;

import com.cookbook.app.entity.Order;
import com.cookbook.app.enumConstant.OrderStatus;
import com.cookbook.app.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController implements OrderService {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public List<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @GetMapping("/customerId/{customerId}")
    public List<Order> getOrderByCustomerId(@PathVariable("customerId") Long customerId) {
        return orderService.getOrderByCustomerId(customerId);
    }

    @GetMapping("/chefId/{chefId}")
    public List<Order> getOrderByChefId(@PathVariable("chefId") Long chefId) {
        return orderService.getOrderByChefId(chefId);
    }

    @GetMapping("/chefAndCustomer")
    public List<Order> getOrderByChefIdAndCustomerId(@RequestParam(value = "chefId") Long chefId,@RequestParam(value = "customerId") Long customerId) {
        return orderService.getOrderByChefIdAndCustomerId(chefId , customerId);
    }

    @GetMapping("/orderStatus/{orderStatus}")
    public List<Order> getOrderByOrderStatus(@PathVariable("orderStatus") OrderStatus orderStatus) {
        return orderService.getOrderByOrderStatus(orderStatus);
    }

    @GetMapping("/chefAndOrderStatus")
    public List<Order> getOrderByChefIdAndStatus(@RequestParam(value = "chefId") Long chefId, @RequestParam(value = "orderStatus") OrderStatus orderStatus) {
        return orderService.getOrderByChefIdAndStatus(chefId , orderStatus);
    }

    @GetMapping("/customerAndOrderStatus")
    public List<Order> getOrderByCustomerIdAndStatus(@RequestParam(value = "customerId") Long customerId, @RequestParam(value = "orderStatus") OrderStatus orderStatus) {
        return orderService.getOrderByCustomerIdAndStatus(customerId , orderStatus);
    }

    @GetMapping("/chefAndCustomerAndOrderStatus")
    public List<Order> getOrderByChefIdCustomerIdAndStatus(@RequestParam(value = "chefId") Long chefId, @RequestParam(value = "customerId") Long customerId, @RequestParam(value = "orderStatus") OrderStatus orderStatus) {
        return orderService.getOrderByChefIdCustomerIdAndStatus(chefId , customerId , orderStatus);
    }

    @GetMapping("/between")
    public List<Order> getOrderBetween(@RequestParam(value = "from") Timestamp from, @RequestParam(value = "to") Timestamp to) {
        return orderService.getOrderBetween(from , to);
    }

    @GetMapping("/chefBetween")
    public List<Order> getOrderByChefIdBetween(@RequestParam(value = "chefId") Long chefId, @RequestParam(value = "from") Timestamp from, @RequestParam(value = "to") Timestamp to) {
        return orderService.getOrderByChefIdBetween(chefId , from , to);
    }

    @GetMapping("/customerBetween")
    public List<Order> getOrderByCustomerIdBetween(@RequestParam(value = "customerId") Long customerId, @RequestParam(value = "from") Timestamp from, @RequestParam(value = "to") Timestamp to) {
        return orderService.getOrderByCustomerIdBetween(customerId , from , to);
    }

    @GetMapping("/chefAndCustomerBetween")
    public List<Order> getOrderByChefIdAndCustomerIdBetween(@RequestParam(value = "chefId") Long chefId, @RequestParam(value = "customerId") Long customerId, @RequestParam(value = "from") Timestamp from, @RequestParam(value = "to") Timestamp to) {
        return orderService.getOrderByChefIdAndCustomerIdBetween(chefId , customerId , from , to);
    }

    @PutMapping("/description")
    public void updateOrderDescription(@RequestParam(value = "id") Long id, @RequestParam(value = "updatedDescription") String updatedDescription) {
        orderService.updateOrderDescription(id , updatedDescription);
    }

    @PutMapping("/orderStatus")
    public void updateOrderOrderStatus(@RequestParam(value = "id") Long id, @RequestParam(value = "updatedOrderStatus") String updatedOrderStatus) {
        orderService.updateOrderOrderStatus(id , updatedOrderStatus);
    }
}
