package com.cookbook.app.api;

import com.cookbook.app.apiException.DuplicateDataException;
import com.cookbook.app.apiException.ItemNotFoundException;
import com.cookbook.app.entity.Chef;
import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;
import com.cookbook.app.service.ChefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("chef")
public class ChefController {

    @Autowired
    private ChefService chefService;

    @GetMapping
    public List<Chef> getAllChefs() {
        return chefService.getAllChefs();
    }

    @GetMapping("/email/{email}")
    public Chef getChefByEmail(@PathVariable("email") String email) throws ItemNotFoundException {
        return chefService.getChefByEmail(email);
    }

    @GetMapping("/mobileNumber/{mobileNumber}")
    public Chef getChefByMobileNumber(@PathVariable("mobileNumber") String mobileNumber) throws ItemNotFoundException {
        return chefService.getChefByMobileNumber(mobileNumber);
    }

    @PostMapping
    public void saveChef(@RequestBody Chef chef) throws DuplicateDataException {
        chefService.saveChef(chef);
    }

    @PutMapping("/email")
    public void updateChefEmail(@RequestParam(value = "email") String email, @RequestParam(value = "updatedEmail") String updatedEmail) throws DuplicateDataException {
        chefService.updateChefEmail(email,updatedEmail);
    }

    @PutMapping("/mobileNumber")
    public void updateChefMobileNumber(@RequestParam(value = "email") String email, @RequestParam(value = "updatedNumber") String updatedNumber) throws DuplicateDataException {
        chefService.updateChefMobileNumber(email , updatedNumber);
    }

    @PutMapping("/address")
    public void updateChefAddress(@RequestParam(value = "email") String email, @RequestParam(value = "updatedAddress") String updatedAddress) {
        chefService.updateChefAddress(email , updatedAddress);
    }

    @PutMapping("/description")
    public void updateChefDescription(@RequestParam(value = "email") String email, @RequestParam(value = "updatedDescription") String updatedDescription) {
        chefService.updateChefDescription(email , updatedDescription);
    }

    @PutMapping("/ethnicity")
    public void updateChefEthnicity(@RequestParam(value = "email") String email, @RequestParam(value = "updatedEthnicity") Ethnicity updatedEthnicity) {
        chefService.updateChefEthnicity(email, updatedEthnicity);
    }

    @PutMapping("/height")
    public void updateChefHeight(@RequestParam(value = "email") String email, @RequestParam(value = "updatedHeight") String updatedHeight) {
        chefService.updateChefHeight(email , updatedHeight);
    }

    @PutMapping("/firstname")
    public void updateChefFirstName(@RequestParam(value = "email") String email, @RequestParam(value = "updatedFirstname") String updatedFirstname) {
        chefService.updateChefFirstName(email, updatedFirstname);
    }

    @PutMapping("/lastname")
    public void updateChefLastName(@RequestParam(value = "email") String email, @RequestParam(value = "updatedLastname") String updatedLastname) {
        chefService.updateChefLastName(email, updatedLastname);
    }

    @PutMapping("/sex")
    public void updateChefSex(@RequestParam(value = "email") String email, @RequestParam(value = "updatedSex") Sex updatedSex) {
        chefService.updateChefSex(email, updatedSex);
    }

    @DeleteMapping("/email")
    public void deleteByEmail(@RequestParam(value = "email") String email) {
        chefService.deleteByEmail(email);
    }

    @DeleteMapping("/mobileNumber")
    public void deleteByMobileNumber(@RequestParam(value = "mobileNumber") String number) {
        chefService.deleteByMobileNumber(number);
    }
}