package com.cookbook.app.repo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderRepositoryTest {


    @Test
    void findByCustomerId() {
    }

    @Test
    void findByChefId() {
    }

    @Test
    void getOrderByChefIdAndCustomerId() {
    }

    @Test
    void getOrderByOrderStatus() {
    }

    @Test
    void getOrderByChefIdAndStatus() {
    }

    @Test
    void getOrderByCustomerIdAndStatus() {
    }

    @Test
    void getOrderByChefIdCustomerIdAndStatus() {
    }

    @Test
    void getOrderBetween() {
    }

    @Test
    void getOrderByChefIdBetween() {
    }

    @Test
    void getOrderByCustomerIdBetween() {
    }

    @Test
    void getOrderByChefIdAndCustomerIdBetween() {
    }

    @Test
    void updateOrderDescription() {
    }

    @Test
    void updateOrderOrderStatus() {
    }
}