package com.cookbook.app.repo;

import com.cookbook.app.entity.Chef;
import com.cookbook.app.enumConstant.Ethnicity;
import com.cookbook.app.enumConstant.Sex;
import com.cookbook.app.service.ChefServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@SpringBootTest
class ChefRepositoryTest {

    @Autowired
    private ChefRepository chefRepository;

    @Test
    void saveChef() {
//        Chef chef = Chef.builder()
//                .address("xxx")
//                .dob(LocalDate.of(2000 , Month.APRIL , 22))
//                .email("shake@gmail.com")
//                .ethnicity(Ethnicity.ASIAN)
//                .firstName("shake")
//                .lastName("rito")
//                .height(173)
//                .mobileNumber("9677018791")
//                .sex(Sex.FEMALE)
//                .build();

        Chef chef = Chef.builder()
                .address("xxx")
                .dob(LocalDate.of(1990, Month.APRIL, 22))
                .email("hughie@gmail.com")
                .ethnicity(Ethnicity.EUROPEAN)
                .firstName("hughie")
                //.lastName("rito")
                .height(193)
                .mobileNumber("9000000000")
                .sex(Sex.MALE)
                .build();
        chefRepository.save(chef);
    }

    @Test
    void getAllChefs() {
        chefRepository.findAll().
                forEach(chef -> System.out.println(chef.toString()));
    }

    @Test
    void getChefByEmail() {
        String email = "hughie@gmail.com";

        chefRepository.findByEmail(email)
                .ifPresentOrElse(
                        chef -> System.out.println(chef.toString())
                        ,
                        () -> System.out.println("No chef is there with emailID : " + email));

//                chefRepository.findAll()
//                        .stream()
//                        .filter(chef -> chef.getEmail().equals(email))
//                        .findFirst()
//                        .ifPresentOrElse(
//                                chef -> {
//                                    System.out.println(chef.toString());
//                                },
//                                () -> System.out.println("Not present")
//                        );
    }

    @Test
    void getChefByMobileNumber() {
        String mobileNumber = "9840786324";

//        chefRepository.findByMobileNumber(mobileNumber)
//                .ifPresentOrElse(
//                        chef -> System.out.println(chef.toString())
//                        ,
//                        () -> System.out.println("No chef is there with mobile number : " + mobileNumber));

        Chef chef = chefRepository.findByMobileNumber(mobileNumber).orElse(null);
        if(chef != null) {
            System.out.println(chef.toString());
        }
        else {
            System.out.println("null");
        }
//        System.out.println(chefRepository.findByMobileNumber(mobileNumber)
//                .map(chef -> {
//                    return chef;
//                })
//                .orElseGet(() -> {return "nulllllllllll";})
//                .toString()
//        );
    }

//    @Test
//    void updateDescription() {
//        String description = "I am a good cook with excellent skills";
//        String email = "shake@gmail.com";
//        chefRepository.updateDescriptionByEmail(email, description);
//    }

    @Test
    void updateEmail() {
        String email = "hughie@gmail.com";
        String updatedEmail = "hugie@gmail.com";
        chefRepository.updateEmailByEmail(email, updatedEmail);
    }

    @Test
    void updateNumber() {
        String number = "9840786324";
        String email = "hugie@gmail.com";
        chefRepository.updateNumberByEmail(email , number);
    }

    @Test
    void updateChefAddress(){
        String updatedAddress = "Mugappair East";
        String email = "shake@gmail.com";
        chefRepository.updateChefAddress(email , updatedAddress);
    }

    @Test
    void updateChefDescription(){
        String updatedDescription = "xxxx";
        String email = "shake@gmail.com";
        chefRepository.updateChefDescription(email , updatedDescription);
    }

    @Test
    void updateChefEthnicity(){
        Ethnicity updatedEthnicity = Ethnicity.EUROPEAN;
        String email = "shake@gmail.com";
      //  System.out.println(updatedEthnicity.toString());
        chefRepository.updateChefEthnicity(email , updatedEthnicity.toString());
    }

    @Test
    void updateChefHeight(){
    }

    @Test
    void updateChefFirstName(){
    }

    @Test
    void updateChefLastName(){
    }

    @Test
    void updateChefSex(){
    }


    @Test
    void deleteChefByEmail(){
        String email = "hughie@gmail.com";
        chefRepository.deleteByEmail(email);
    }

    @Test
    void deleteChefByMobileNumber(){
        String mobileNumber = "9000000000";
        chefRepository.deleteByMobileNumber(mobileNumber);
    }

    @Test
    void testChefValidatorInterface(){
        Chef chef = Chef.builder()
                .address("xxx")
                .dob(LocalDate.of(1990, Month.APRIL, 22))
                .email("hughie@gmail.com")
                .ethnicity(Ethnicity.EUROPEAN)
                .firstName("hughie")
                //.lastName("rito")
                .height(193)
                .mobileNumber("0123456789")
                .sex(Sex.MALE)
                .build();

        ChefServiceImpl chefService = new ChefServiceImpl();
//        System.out.println("\n\n\n" + chefService.testChefValidatorinterface(chef) + "\n\n\n");
    }

    @Test
    void testFindByEmailAndMobilenumber(){
        System.out.println("llllllllllllllllllllllllllllllllllllllllllllllllllllll\n");
        chefRepository.findByEmailAndMobileNumber("shake@gmail.com" , "9677018791")
                //.stream()
                .forEach(System.out::println);
    }

    @Test
    void testFindByEmailAndMobileNumberAndFirstName(){
        System.out.println("llllllllllllllllllllllllllllllllllllllllllllllllllllll\n");
        chefRepository.findByEmailAndMobileNumberAndFirstName("shake@gmail.com" , "9677018791" , "shakthi")
                //.stream()
                .forEach(System.out::println);
    }
}