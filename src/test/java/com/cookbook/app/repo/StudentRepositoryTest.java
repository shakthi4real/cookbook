//package com.cookbook.app.repo;
//
//import com.cookbook.app.dummy.StudentRepository;
//import com.cookbook.app.dummy.Guardian;
//import com.cookbook.app.dummy.Student;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.time.LocalDate;
//import java.time.Month;
//
//@SpringBootTest
//class StudentRepositoryTest {
//
//    @Autowired
//    private StudentRepository studentRepository;
//
//    @Test
//    public void savaStudent(){
////        Student shakthi = Student.builder()
////                .name("Shakthi")
////                .emailId("shakthi247735@gmail.com")
////                .dob(LocalDate.of(2001 , Month.APRIL , 22))
////                .guardianName("Ravi")
////                .guardianEmail("ravi@gmail.com")
////                .guardianMobile("967718791")
////                .build();
////        studentRepository.save(shakthi);
//
////        Student snegha = Student.builder()
////                .name("Snegha")
////                .emailId("snegha@gmail.com")
////                .dob(LocalDate.of(2000 , Month.DECEMBER , 30))
////                .guardianName("Ramesh")
////                .guardianEmail("ramesh@gmail.com")
////                .guardianMobile("967718791")
////                .build();
////        studentRepository.save(snegha);
//
////        Student ram = Student.builder()
////                .name("ram")
////                .emailId("ram@gmail.com")
////                .dob(LocalDate.of(2000 , Month.DECEMBER , 30))
////                .guardianName("ram")
////                .guardianEmail("ram@gmail.com")
////                .guardianMobile("967718791")
////                .build();
////        studentRepository.save(ram);
//    }
//
//    @Test
//    public void savaStudentWithGuardian(){
//        Guardian guardian = Guardian.builder()
//                .name("ravindran")
//                .emailId("ravindran@gmail.com")
//                .mobileNumber("00000000000")
//                .build();
//
//        Student viswa = Student.builder()
//                .name("Viswa")
//                .emailId("Viswa@gmail.com")
//                .dob(LocalDate.of(1990 , Month.DECEMBER , 30))
//                .guardian(guardian)
//                .build();
//        studentRepository.save(viswa);
//    }
//
//    @Test
//    public void printStudent(){
////        List<Student> students= studentRepository.findAll();
////        System.out.println("jjjjjjjjjjjjjjjjjjjjjjjjjj");
////         for(Student student : students){
////            System.out.println(student.toString());
////        }
//        studentRepository.findAll().stream().forEach(student -> System.out.println(student.toString()));
//    }
//
//    @Test
//    public void updateStudentByEmailId(){
//        //String queryGuardianName = "ravindran";
////        String queryEmailId = "Viswa@gmail.com";
//        String queryEmailId = "sibii@gmail.com";
//        Guardian guardian = Guardian.builder()
//                .name("vasan")
//                .emailId("vasan@gmail.com")
//                .mobileNumber("7777777777")
//                .build();
//
//        Student sibi = Student.builder()
//                .name("sibi")
//                .emailId("sibi@gmail.com")
//                .dob(LocalDate.of(1994 , Month.DECEMBER , 30))
//                .guardian(guardian)
//                .build();
//
////        studentRepository.updateStudentByEmailId(sibi.getEmailId(), guardian.getMobileNumber() , queryEmailId
////               // sibi.getName() ,
////              //  sibi.getEmailId(),
////              ,  sibi.getDob()
////             //   guardian.getName() ,
////              //  guardian.getEmailId() ,
////                );
//
//        studentRepository.updateStudentByEmailId(queryEmailId,
//                 sibi.getName() ,
//                  sibi.getEmailId(),
//                 sibi.getDob(),
//                   guardian.getName() ,
//                  guardian.getEmailId() ,
//                guardian.getMobileNumber()
//        );
//    }
//}